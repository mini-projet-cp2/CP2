
global code
global inverse

def cp1(code):##On définit la fonction cp1 qui renvera sous la forme d'une chaine son complement à 1
    inverse = ''
    for i in code:
        if i == '0':
            inverse += '1'##Ici on inverse les 1 et les 0 pour calculer son complément à 1
        else:
            inverse += '0'
    return inverse

def cp2(code):##On définit la fonction cp2 afin de renvoyer le complement à 2 du nombre binaire
    a = int(cp1(code))##Le nombre binaire entré par l'utilisateur
    b = int('1')##La valeur 1 qui nous permet d'effectuer le complément à 2
    retenue = 0 ##|
    resultat = 0##|On initialise des variables afin d'effectuer le calcul
    position = 0##|
    while a > 0 or b > 0 or retenue > 0:##On met en place une boucle while afin de calculer le résultat s'y il y a une retenue
        chiffre_a = a % 10
        chiffre_b = b % 10
        a = a // 10
        b = b // 10
        somme = chiffre_a + chiffre_b + retenue
        retenue = somme // 2##On vérifie si des retenues sont présentes ou pas
        resultat += (somme % 2) * 10**position##Cela permet de faire naviguer la retenue vers la gauche
        position += 1
    return resultat

##__________________________________________________________
##|Nombre relatifs décimaux     |    Codage cp2 sur 16 bits|
##|_____________________________|__________________________|
##|       -2357                 |    1011011001011         |
##|_____________________________|__________________________|
##|       45681                 |    10100110110001111     |
##|_____________________________|__________________________|
##|       -159                  |    101100001             |
##| ____________________________|__________________________|
##|       2189                  |    1011101110011         |
##|_____________________________|__________________________|
##|       -167                  |    101011001             |
##|_____________________________|__________________________|

